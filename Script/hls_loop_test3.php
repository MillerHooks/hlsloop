<?php
// disable execution time limit
set_time_limit(0);


$doLoop = true;


// how long to wait bewteen each update of the playlst
$waitSeconds = 4;

// which segement of the video are we dealing with.
$segmentNum = 0;

// the media sequence number is not the same as 
// the segment number.  The media sequence number
// always needs to increase by 1 every time we update 
// the playlist.
$mediaSequence = 0;

// The number of segments we want to add to the looping playlist
$numOfSegmentsToAdd = 4;


// an array of all video IDs to be played in the stream.
//$streamVideoIDs = ["160028", "160029"];
$streamVideoIDs = ["160028", "160029"];

// the index of the currently playing video in the streamVideoIDs
$streamVideoIndex = 0;

$channelId = "test_channel_loop";

$nextVideoStartSegmentNum = 0;


function getSegmentsFromPlaylist($playlistUrl, $startSegmentNum, $numSegments){

	$sourcePlaylist = new SplFileObject($playlistUrl);

	// skip the first 6 lines (those are header info)
	// and jump to segment we want
	$jumpToLine = 6 + ($startSegmentNum * 2);
	
	$sourcePlaylist->seek($jumpToLine);

	$segmentsArray = array();

	$currentLineIndex = 0;


	while ($currentLineIndex < ($numSegments * 2)) {		

		if ( strpos($sourcePlaylist->current(), '#EXT-X-ENDLIST') === 0){
			echo "END OF VIDEO...\n";
			$segmentsArray[] = "#EXT-X-DISCONTINUITY\n";
			break;
		}else{
			$segmentString = "";
			$segmentString .= $sourcePlaylist->current();
		    $sourcePlaylist->next();
		    $currentLineIndex++;
			
			$segmentString .= $sourcePlaylist->current();
			$sourcePlaylist->next();
			$currentLineIndex++;
			
			$segmentsArray[] = $segmentString;
		}



		
		
	}

	return $segmentsArray;

}




function writeChannelPlaylist($firstVideoSegements, $bandwidthType){
	echo "writeChannelPlaylist {$bandwidthType}....\n";

	
	global $channelId;
	global $mediaSequence;
	$outFile = new SplFileObject("{$channelId}_{$bandwidthType}.m3u8", "w");
	
	$segmentsOutput = "";

	// since we only pull the segments form the hi bandwith playlist, 
	// we need to replace "hi" with the right bandwith type.
	foreach ($firstVideoSegements as $segmentString) {
	    $segmentsOutput .= str_replace("_medium", "_".$bandwidthType, $segmentString);
	}

	$updatedPlaylist = <<<EOD
#EXTM3U

#EXT-X-VERSION:3
#EXT-X-MEDIA-SEQUENCE:$mediaSequence
#EXT-X-TARGETDURATION:5

$segmentsOutput 

EOD;

	if ($bandwidthType === "medium"){
		echo "$updatedPlaylist\n\n";
	}
	
	$outFile->fwrite ($updatedPlaylist);
}

$prependDiscTag = false;

while ($doLoop) {

	// the id of the video we want to add to the channel playlist
	$videoId = $streamVideoIDs[$streamVideoIndex];

	$nextSegments = getSegmentsFromPlaylist("{$videoId}_video_medium.m3u8", $segmentNum, $numOfSegmentsToAdd);

	$segmentsLength = count($nextSegments);

	$hasEndOfVideo = ($nextSegments[$segmentsLength-1] === "#EXT-X-DISCONTINUITY\n");


	if($hasEndOfVideo){

		//check to see if  only have the #EXT-X-DISCONTINUITY tag,
		//if so move the streamVideoIndex to the next video.
		
		if($segmentsLength == 1){

			$streamVideoIndex++;

			if($streamVideoIndex == $numVideos){
				$streamVideoIndex = 0;
			}
			$segmentNum = -1;//$nextVideoStartSegmentNum-1;

			$nextVideoStartSegmentNum = 0;

			$prependDiscTag = true;
		}else{
			$numVideos = count($streamVideoIDs);

			$nextVideoIndex = $streamVideoIndex+1;

			if($nextVideoIndex == $numVideos){
				$nextVideoIndex = 0;
			}
			
			$nextVideoId = $streamVideoIDs[$nextVideoIndex];
			$numRemaining = $numOfSegmentsToAdd - ($segmentsLength-1);

			$nextVideoSegments = getSegmentsFromPlaylist("{$nextVideoId}_video_medium.m3u8", 0, $numRemaining);

			$nextVideoStartSegmentNum++;

			$nextSegments = array_merge($nextSegments, $nextVideoSegments);
		}
	}//else{
		//if($prependDiscTag){
		//	array_unshift($nextSegments, "#EXT-X-DISCONTINUITY\n") ;
			//$prependDiscTag = false;
		//}
	//}

	if($segmentsLength > 1){
		writeChannelPlaylist($nextSegments, "low");
		writeChannelPlaylist($nextSegments, "medium");
		writeChannelPlaylist($nextSegments, "hi");
	}


	
	$segmentNum++;
	echo "moved to segment : $segmentNum\n";
	$mediaSequence++;
	sleep($waitSeconds);
}

?>