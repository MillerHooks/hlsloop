# Nginx can serve FLV/MP4 files by pseudo-streaming way without any specific media-server software. 
# To do the custom build we use 2 modules: --with-http_secure_link_module --with-http_flv_module
# This module "secure-link" helps you to protect links from stealing away.
#
# NOTE: see more details at coderwall: http://coderwall.com/p/3hksyg

cd /usr/src/
wget http://nginx.org/download/nginx-1.5.13.tar.gz
wget http://downloads.sourceforge.net/project/pcre/pcre/8.32/pcre-8.32.tar.gz
wget http://www.openssl.org/source/openssl-1.0.1g.tar.gz


ls -la
tar xzvf ./nginx-1.5.13.tar.gz
tar xzvf pcre-8.32.tar.gz
tar xzvf openssl-1.0.1g.tar.gz

cd nginx-1.5.13 && ./configure --with-pcre=/usr/src/pcre-8.32 --without-mail_pop3_module --without-mail_smtp_module --without-mail_imap_module --with-http_stub_status_module --with-http_secure_link_module --with-http_flv_module --with-http_mp4_module --without-http_gzip_module
make && make install
