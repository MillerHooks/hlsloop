server {
    listen 80;
    listen 443 ssl;
    #	listen [::]:80 default_server ipv6only=on;
    server_name localhost;

    root /usr/www/looper/;
    index index.html;

    access_log /var/log/nginx/access.looper.log;

	ssl_certificate /etc/nginx/ssl/nginx.crt;
    ssl_certificate_key /etc/nginx/ssl/nginx.key;

	location / {
	    hls;
        hls_fragment            5s;
        hls_buffers             10 10m;
        hls_mp4_buffer_size     1m;
        hls_mp4_max_buffer_size 5m;

		# First attempt to serve request as file, then
		# as directory, then fall back to displaying a 404.
		try_files $uri $uri/ =404;
		# Uncomment to enable naxsi on this location
		# include /etc/nginx/naxsi.rules
		autoindex on;



		if ($request_method = 'OPTIONS') {
                    	add_header 'Access-Control-Allow-Origin' '*';
                    	#
                    	# Om nom nom cookies
                    	#
                    	add_header 'Access-Control-Allow-Credentials' 'true';
                    	add_header 'Access-Control-Allow-Methods' 'GET, POST, OPTIONS';
                    	#
                    	# Custom headers and headers various browsers *should* be OK with but aren't
                    	#
                    	add_header 'Access-Control-Allow-Headers' 'DNT,X-CustomHeader,Keep-Alive,User-Agent,X-Requested-With,If-Modified-Since,Cache-Control,Content-Type';
                    	#
                    	# Tell client that this pre-flight info is valid for 20 days
                    	#
            	        add_header 'Access-Control-Max-Age' 1728000;
            	        add_header 'Content-Type' 'text/plain charset=UTF-8';
            	        add_header 'Content-Length' 0;
                    	return 204;
                }
            	if ($request_method = 'POST') {
            	        add_header 'Access-Control-Allow-Origin' '*';
            	        add_header 'Access-Control-Allow-Credentials' 'true';
                    	add_header 'Access-Control-Allow-Methods' 'GET, POST, OPTIONS';
                    	add_header 'Access-Control-Allow-Headers' 'DNT,X-CustomHeader,Keep-Alive,User-Agent,X-Requested-With,If-Modified-Since,Cache-Control,Content-Type';
                }
                if ($request_method = 'GET') {
                    	add_header 'Access-Control-Allow-Origin' '*';
                    	add_header 'Access-Control-Allow-Credentials' 'true';
                    	add_header 'Access-Control-Allow-Methods' 'GET, POST, OPTIONS';
                    	add_header 'Access-Control-Allow-Headers' 'DNT,X-CustomHeader,Keep-Alive,User-Agent,X-Requested-With,If-Modified-Since,Cache-Control,Content-Type';
                }
	}

	# pass the PHP scripts to FastCGI server listening on 127.0.0.1:9000
             location ~ [^/]\.php(/|$) {
               fastcgi_split_path_info ^(.+?\.php)(/.*)$;
               if (!-f $document_root$fastcgi_script_name) {
                 return 404;
               }
               fastcgi_pass php:9000;
               fastcgi_index index.php;
               fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
               include fastcgi_params;
             }
}